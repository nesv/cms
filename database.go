package cms

import (
	"fmt"
	"net/url"
	"time"
)

type ErrUnsupportedDB struct {
	scheme string
}

func (err ErrUnsupportedDB) Error() string {
	return fmt.Sprintf("unsupported database scheme: %v", err.scheme)
}

type ErrNoSuchPage struct {
	slug   string
	pageid string
}

func (err ErrNoSuchPage) Error() string {
	if err.pageid != "" {
		return fmt.Sprintf("no such page: %v", err.pageid)
	}
	if err.slug != "" {
		return fmt.Sprintf("no such page: %v", err.slug)
	}

	return "no such page"
}

func OpenDatabase(urlStr string) (Database, error) {
	u, err := url.Parse(urlStr)
	if err != nil {
		return nil, err
	}

	switch scheme := u.Scheme; scheme {
	case "bolt":
		return openBoltDB(u.Path)
	default:
		return nil, ErrUnsupportedDB{scheme: scheme}
	}
}

type Database interface {
	PageStore
}

type PageStore interface {
	GetPage(id interface{}) (*Page, error)
	GetPageBySlug(string) (*Page, error)
}

type Page struct {
	ID            interface{}
	Title         string
	Slug          string
	Content       string
	Author        Author
	PublishedDate time.Time
	LastUpdated   time.Time
}

type Author struct {
	Name string
}
