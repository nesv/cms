# Contribution guidelines

If you would like to contribute to the development of cms, you are more than
welcome to fork the code, and open up a merge request.

## Code of conduct

Don't be a jerk.

If you are being unreasonable, you are free to fork this code, and maintain that
fork in your own repository.