package main

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/nesv/cms"
)

var (
	mainCmd = &cobra.Command{
		Use:   "cms",
		Short: "A simple, easy-to-use content management system",
	}

	serveCmd = &cobra.Command{
		Use:   "serve",
		Short: "Run the HTTP(s) server",
		Run:   startServer,
	}
)

func main() {
	// Set up the command-line flags for the "cms" command.
	var defaultTemplateDir string
	for _, dir := range strings.Split(os.Getenv("GOPATH"), ":") {
		tplDir := filepath.Join(dir, "src", "gitlab.com", "nesv", "cms", "templates", "default")
		if info, err := os.Stat(tplDir); err != nil && os.IsNotExist(err) {
			continue
		} else if err != nil && os.IsPermission(err) {
			continue
		} else if err == nil && info.IsDir() {
			defaultTemplateDir = tplDir
			break
		}
	}
	serveCmd.Flags().StringP("template-dir", "t", defaultTemplateDir, "Path to the directory containing templates")
	if pwd, err := os.Getwd(); err != nil {
		serveCmd.Flags().StringP("database-url", "d", "bolt:///tmp/cms.db", "Database URL")
	} else {
		serveCmd.Flags().StringP("database-url", "d", fmt.Sprintf("bolt://%s/cms.db", pwd), "Database URL")
	}
	serveCmd.Flags().StringP("bind", "b", ":3030", "addr:port to listen on")
	serveCmd.Flags().String("tls-cert", "", "Path to TLS cert file")
	serveCmd.Flags().String("tls-key", "", "Path to TLS cert key file")
	serveCmd.Flags().String("cache", "", "Specify the caching method (default: no caching)")

	// Bind the flags to configuration values in Viper, so that they can
	// be merged together with a configuration file, and environment
	// variables.
	viper.BindPFlags(serveCmd.Flags())

	// Run!
	mainCmd.AddCommand(serveCmd)
	mainCmd.Execute()
}

func startServer(cmd *cobra.Command, args []string) {
	// Check to see if the template directory was specified.
	if !viper.IsSet("template-dir") {
		log.Fatalln("No template directory specified; please specify one using the -t/--template-dir flag")
	}
	log.Infof("Loading templates from %q", viper.GetString("template-dir"))
	tpl, err := template.ParseGlob(filepath.Join(viper.GetString("template-dir"), "*.html"))
	if err != nil {
		log.Fatalln(err)
	}

	db, err := cms.OpenDatabase(viper.GetString("database-url"))
	if err != nil {
		log.Fatalln(err)
	}

	staticHandler := http.StripPrefix("/static", http.FileServer(
		http.Dir(filepath.Join(viper.GetString("template-dir"), "static")),
	))

	r := http.NewServeMux()
	r.Handle("/static/", staticHandler)
	r.Handle("/", cms.PageHandler{
		Template: tpl,
		DB:       db,
	})

	if tlsCert, tlsKey := viper.GetString("tls-cert"), viper.GetString("tls-key"); tlsCert != "" && tlsKey != "" {
		log.Infoln("HTTPS listener started on", viper.GetString("bind"))
		log.Fatalln(http.ListenAndServeTLS(
			viper.GetString("bind"),
			tlsCert,
			tlsKey,
			r,
		))
	}
	log.Infoln("HTTP listener started on", viper.GetString("bind"))
	log.Fatalln(http.ListenAndServe(viper.GetString("bind"), r))
}
