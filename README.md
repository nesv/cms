# cms

cms aims to be a well-documented, easy-to-customize content management system.

## Requirements

- Go >=1.5

You will need to have the `GO15VENDOREXPERIMENT` environment variable set in
your current shell. If you run:

	$ echo $GO15VENDOREXPERIMENT
	1

and you see anything other than `1` for output, you will need to set it, like
so, in your shell's .rc file:

	## bash and zsh
	export GO15VENDOREXPERIMENT=1

## Installing it with `go get`

	$ go get -v -u gitlab.com/nesv/cms/cmd/cms

## Building it (from a git-clone)

	$ go get -d gitlab.com/nesv/cms
	$ cd $GOPATH/src/gitlab.com/nesv/cms
	$ make

The resulting `cms` binary will be in `$GOPATH/src/gitlab.com/nesv/cms/bin`,
and you can copy it to any directory that is in your `$PATH`.

## Running it

	$ cms serve --database-url="bolt:///tmp/cms.db"
	INFO[0000] Loading templates from "/home/nesv/src/gitlab.com/nesv/cms/templates/default"
	INFO[0000] HTTP listener started on :3030

## Databases

By deafult, cms uses [bolt](https://github.com/boltdb/bolt) for a database
to store content in. While this is great for low-traffic sites, it may not be
the best-suited database under load, especially if you do not have caching
configured.

# TODO

## 0.1.0

- Configuration (file, environment variables)
- Groupcache (for caching rendered pages)
- Theme/template design documentation
