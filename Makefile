all: bin/cms

bin/%: $(shell find . -iname "*.go")
	go build -o $@ ./cmd/$*

clean:
	rm -rf bin
