package cms

import (
	"html/template"
	"log"
	"net/http"
	"strings"
)

func renderPage(w http.ResponseWriter, page *Page) error {
	return nil
}

type PageHandler struct {
	DB       Database
	Template *template.Template
}

func (h PageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch m := r.Method; m {
	case "GET":
		if r.URL.Path == "/" {
			h.serveIndex(w, r)
			return
		}
		h.servePage(w, r)

	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func (h PageHandler) serveIndex(w http.ResponseWriter, r *http.Request) {
	if err := h.Template.ExecuteTemplate(w, "index", nil); err != nil {
		log.Printf("error rendering index template: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h PageHandler) servePage(w http.ResponseWriter, r *http.Request) {
	page, err := h.DB.GetPageBySlug(strings.TrimPrefix(r.URL.Path, "/"))
	if err != nil {
		if _, ok := err.(ErrNoSuchPage); ok {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}

		log.Println("error:", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := renderPage(w, page); err != nil {
		log.Println("failed to render page:", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Error(w, "Not implemented", http.StatusNotImplemented)
}
