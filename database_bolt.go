package cms

import (
	"encoding/json"
	"fmt"

	"github.com/boltdb/bolt"
)

var (
	boltBuckets = []string{
		"slugs",
		"pages",
	}
)

func openBoltDB(dbPath string) (Database, error) {
	db, err := bolt.Open(dbPath, 0644, nil)
	if err != nil {
		return nil, err
	}
	err = db.Update(func(tx *bolt.Tx) error {
		for _, bucketName := range boltBuckets {
			if _, err := tx.CreateBucketIfNotExists([]byte(bucketName)); err != nil {
				return err
			}
		}
		return nil
	})
	return &boltdb{db: db}, err
}

type boltdb struct {
	db *bolt.DB
}

func (db *boltdb) GetPage(id interface{}) (*Page, error) {
	pageId, ok := id.([]byte)
	if !ok {
		return nil, fmt.Errorf("unable to coerce page id to []byte")
	}

	var page Page
	err := db.db.View(func(tx *bolt.Tx) error {
		p := tx.Bucket([]byte("pages")).Get([]byte(pageId))
		if p == nil {
			return ErrNoSuchPage{pageid: string(pageId)}
		}

		return json.Unmarshal(p, &page)
	})

	return &page, err
}

func (db *boltdb) GetPageBySlug(slug string) (*Page, error) {
	var pid []byte
	err := db.db.View(func(tx *bolt.Tx) error {
		pid = tx.Bucket([]byte("slugs")).Get([]byte(slug))
		return nil
	})
	if err != nil {
		return nil, err
	}
	if pid == nil {
		return nil, ErrNoSuchPage{slug: slug}
	}

	return db.GetPage(pid)
}
